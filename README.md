# SMWCentral Integration

This project contains a browser extension that adds a "Play" button to SMW hacks on SMWCentral,
as well as a simple script that acts as a URL handler so that clicking the button invokes SMWCli.
